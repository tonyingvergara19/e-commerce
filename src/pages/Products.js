import {useState, useEffect} from 'react'
import ProductCard from './../components/ProductCard'
import {Container} from 'react-bootstrap'

export default function Products (){
	const [productsCollection,setProductCollection] = useState([]);

	useEffect(()=>{

		fetch('https://e-commerce-capstone-2.herokuapp.com/products/active').then(res => res.json()).then(convertedData =>{
			console.log(convertedData)
			setProductCollection(convertedData.map(product =>{
				return(

					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
	},[]);
	return(
		
		
		<Container>
			{productsCollection}
		</Container>
		
		
	)
}