import {Row, Col, Card, Button, Container} from 'react-bootstrap'
import { useState, useEffect} from 'react'
import {Link, useParams} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Product(){
	const [productInfo,setProductInfo] = useState({
		name: null,
		description: null,
		price: null
	})
	console.log(useParams())
	const {id} = useParams()
	console.log(id)
	useEffect (() => {

		fetch(`https://e-commerce-capstone-2.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData =>{ 
			console.log(convertedData);
			setProductInfo({
				name: convertedData.name,
				type: convertedData.type,				
				description: convertedData.description,
				price: convertedData.price
			})
		})

	},[id])
	const AddtoCart = () => {
		return(
			Swal.fire(
			{	icon: 'success',
				title: 'Added to Cart',
				
			})
		);
	};
	return(
	<>
	
	<Row>
		<Col>
			<Container>
			<Card className="text-center">
				<Card.Body>
					<Card.Title>
						<h2>{productInfo.name}</h2>
					</Card.Title>
					<Card.Subtitle>
						<h6 className="my-4">Type</h6>
						</Card.Subtitle>
					<Card.Text>{productInfo.type}
					</Card.Text>
					<Card.Subtitle>
						<h6 className="my-4">Description</h6>
						</Card.Subtitle>
					<Card.Text>{productInfo.description}
					</Card.Text>
					
					<Card.Subtitle>
						<h6 className="my-4">PHP: {productInfo.price}</h6>
						</Card.Subtitle>
				</Card.Body>
				
				<Button variant= "success" className="btn-danger" onClick={AddtoCart}>
					Add to Cart
				</Button>
				<Link className="btn btn-success btn-block mb-5" to ="/login">Login to Continue Shopping
					
				</Link>

				
			</Card>
			</Container>
		</Col>
	</Row>
	</>
	)
}
