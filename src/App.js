
import AppNavBar from './components/AppNavBar'
import './App.css';
import  {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import ProductView from './pages/ProductView'
import Error from './pages/Error'
import {UserProvider} from './UserContext'
import {useState, useEffect} from 'react';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
const unsetUser = () => {
  localStorage.clear()
  setUser({
    id:null,
    isAdmin: null
  })
}
useEffect(()=>{
  let token = localStorage.getItem('accessToken');
    // console.log(token)
    fetch('https://e-commerce-capstone-2.herokuapp.com/users/details',{
      headers:{
        Authorization: `Bearer ${token}`
      }
    }).then(res =>res.json()).then(convertedData =>{ 
      // console.log(convertedData)

      if (typeof convertedData._id !== "undefined") {
          setUser({
            id: convertedData._id,
            isAdmin: convertedData.isAdmin
          });
         // console.log(user)
    } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
    
},[user])
  return (
    <UserProvider value ={{user, setUser ,unsetUser}}>
    <Router>
    <AppNavBar/>
    <Routes>
    <Route path= '/' element={<Home/>} />
    <Route path= '/register' element={<Register/>} />
    <Route path= '/login' element={<Login/>} />
    <Route path= '/logout' element={<Logout/>}/>
    <Route path= '/products' element={<Products/>} />

    <Route path= '/product/view/:id' element={<ProductView/>}/>
    <Route path= '*' element={<Error/>} />
    </Routes>
   

    </Router>
    </UserProvider>
  
   

  );
}

export default App;
