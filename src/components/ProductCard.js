import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default function Highlights({productProp}){
	return( 

		<Card className ="productCard m-4 d-md-inline-flex d-sm-inline-flex" >
			<Card.Body>
				<Card.Title>{productProp.name}</Card.Title>
				<Card.Text>{productProp.type}</Card.Text>
				<Card.Text>{productProp.description}</Card.Text>
				<Card.Text>P{productProp.price}</Card.Text>
				<Link to={`/product/view/${productProp._id}`} className="btn btn-danger">View Product</Link>
			</Card.Body>
		</Card>

		)}