import {useContext} from 'react'
import {Navbar, Nav, Container,Image,NavDropdown} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import userContext from '../UserContext'

function AppNavBar(){
	const {user} = useContext(userContext)
	return(
			<Navbar className='Navbar fs-1' expand='lg' variant='light'>
			<Container>
			<Navbar.Brand><Image className='backgroundlogo ml-auto' src='logo.png' fluid style={{height:'200px',width:'200px'}}/></Navbar.Brand>
			<Navbar.Toggle aria-controls = "responsive-navbar-nav" />
			<Navbar.Collapse >
			
			<Nav className='ml-auto'>
			<Link to ='/' className='nav-link'>Home</Link>
			{user.id !== null ?
				<Link to ='/logout' className='nav-link'>Logout</Link>
				:
			
				<>
				<Link to ='/login' className='nav-link'>Login</Link>
				<Link to ='/register' className='nav-link'>Register</Link>
				</>
				}
				
			<NavDropdown  title = 'Products' className='NavDropdown'>
						
				<NavDropdown.Item href="/products">Overview</NavDropdown.Item>
				<NavDropdown.Item href="/product/view">View Product</NavDropdown.Item>						
				<NavDropdown.Item href="/product/add">Add Product</NavDropdown.Item>	
				<NavDropdown.Item href="/product/update">Update</NavDropdown.Item>
			</NavDropdown>
				<Link to ='/products/addtocart' className='nav-link'>Buy Now</Link>
			</Nav>
			</Navbar.Collapse>				
			</Container>

			</Navbar>
		)
}
export default AppNavBar;